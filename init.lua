dawn_start = tonumber(minetest.settings:get("day_cycles.dawn_start")) or 4625
dawn_end = tonumber(minetest.settings:get("day_cycles.dawn_end")) or 6125
day_light = tonumber(minetest.settings:get("day_cycles.day_light")) or 1.0
night_light = tonumber(minetest.settings:get("day_cycles.night_light")) or 0.15

dofile(minetest.get_modpath("day_cycles").."/api.lua")

day_cycles.set_day_brightness(day_light)
day_cycles.set_night_brightness(night_light)
day_cycles.set_dawn(dawn_start, dawn_end)

minetest.register_on_joinplayer(function(player)
	-- Override the first time when the player joins

	player:override_day_night_ratio(day_cycles.current_ratio)
end)

minetest.register_globalstep(function(dtime)
	-- Update the current day/night ratio

	local current_daynight_ratio = day_cycles.daynight_ratio(minetest.get_timeofday() * 24000)
	local stored_ratio = day_cycles.current_ratio

	if current_daynight_ratio ~= stored_ratio then
		for _,player in ipairs(minetest.get_connected_players()) do
			day_cycles.current_ratio = current_daynight_ratio
			player:override_day_night_ratio(current_daynight_ratio)
		end
	end
end)
