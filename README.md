# Day Cycles

Day Cycles is a mod for [Minetest](http://www.minetest.net/).

It lets the player chooses the time of sunrise and sunset, and the brightness during day and night.

## Installation

To use this mod, simply put the folder in your game's `mods/` folder. For example, to use it with the default Minetest game, you should put this in `minetest/games/minetest_game/mods`.

## Usage

By default, this mod doesn't do anything visible. The default values are the same as in the engine.
But you can check 4 parameters to change the brightness in the game.

The parameters can be set in the `minetest.conf` config file.

There are 2 parameters to set the moment when the brightness should switch between day and night.
These parameters are:

- `day_cycles.dawn_start`: the start of the switch between night and day
- `day_cycles.dawn_end`: the end of the switch

These values are simetric for the evening.
So, given there is 24000 units of time in a day, if `dawn_start` equals `2000` and `dawn_end` equals `4000`, the sunrise will be between `2000` and `4000`, and the sunset between `20000` and `22000`.


The 2 other parameters specify the brightness during night and day. The brightness is a number between `0.0` and `1.0`.
These parameters are:

- `day_cycles.day_light`: the brightness during the day
- `day_cycles.night_light`: the brightness during the night

## Mod API

You can easily use this mod in your own mod. If you want to change the day or night brightness, you can use:

- `day_cycles.set_day_brightness(br)` to change the brightness during the day to `br`.
- `day_cycles.set_night_brightness(br)` to change the brightness during the night to `br`.

To change the moment of dawn, you can use:

- `day_cycles.set_dawn(dawn_start, dawn_end)` with `dawn_start` the start of the dawn and `dawn_end` its end.

And you can also directly access the `day_cycles.dawn_start`, `day_cycles.dawn_end`, `day_cycles.day_light` and `day_cycles.night_light` variables.

## Notes

This mod only controls the brightness during the day and night. It won't affect other visual effects such as the position of the sun, or the red cloud during sunrise and sunset.
It won't affect the spawn of mobs that are restricted to day or night.
