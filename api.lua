day_cycles = {
	dawn_start = 4625,
	dawn_end = 6125,
	day_light = 1,
	night_light = 0.15,
	current_ratio = 1
}

function round(x, n)
	-- Round a number to n decimals
	n = math.pow(10, n or 0)
	x = x * n
	if x >= 0 then x = math.floor(x + 0.5) else x = math.ceil(x - 0.5) end
	return x / n
end

function day_cycles.daynight_ratio(clock_time)
	-- Time is the time of day return by minetest.get_timeofday()
	-- Return a value between 0.15 and 1. 0.15 is dark and 1 is bright
	-- These values are based on the default daynight ratio in the engine, see minetest/src/daynightratio.h

	if clock_time >= (24000 - day_cycles.dawn_start) or clock_time <= day_cycles.dawn_start then
		return day_cycles.night_light
	end
	if clock_time <= (24000 - day_cycles.dawn_end) and clock_time >= day_cycles.dawn_end then
		return day_cycles.day_light
	end
	local abs_time = 12000 - math.abs(clock_time - 12000)
	local a = (day_cycles.day_light - day_cycles.night_light) / (day_cycles.dawn_end - day_cycles.dawn_start)
	local b = day_cycles.night_light - a * day_cycles.dawn_start
	return round(abs_time*a + b, 2)
end


function day_cycles.set_dawn(dawn_start, dawn_end)
	-- Set the beginning and end of dawn, should be between 0 and 12000
	-- dawn_end must be greater than dawn_start

	if dawn_start < 0 then
		dawn_start = 0
	end
	if dawn_start > 11999 then
		dawn_start =11999
	end
	dawn_end = dawn_end or dawn_start + 1500
	if dawn_end <= dawn_start then
		dawn_end = dawn_start + 1
	end
	if dawn_end > 12000 then
		dawn_end = 12000
	end
	day_cycles.dawn_start = dawn_start
	day_cycles.dawn_end = dawn_end

end



function day_cycles.set_day_brightness(brightness)
	-- Brightness is the brightness during the day, should be between 0 and 1

	if brightness <= 1 and brightness >= 0 then
		day_cycles.day_light = brightness
	elseif brightness > 1 then
		day_cycles.day_light = 1
	else
		day_cycles.day_light = 0
	end
end


function day_cycles.set_night_brightness(brightness)
	-- Brightness is the brightness during the night, should be between 0 and 1

	if brightness <= 1 and brightness >= 0 then
		day_cycles.night_light = brightness
	elseif brightness > 1 then
		day_cycles.night_light = 1
	else
		day_cycles.night_light = 0
	end
end
