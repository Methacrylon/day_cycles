
_G.day_cycles = {} --_
require("api")

-- Tests
describe("daynight_ratio", function()
	it("day/night ratio at midnight", function()
		assert.equals(0.15, day_cycles.daynight_ratio(0))
	end)

	it("day/night ratio at noon", function()
		assert.equals(1, day_cycles.daynight_ratio(12000))
	end)

	it("day/night ratio just before dawn", function()
		assert.equals(0.15, day_cycles.daynight_ratio(4625))
	end)

	it("day/night ratio just after dawn", function()
		assert.equals(1, day_cycles.daynight_ratio(6125))
	end)

	it("day/night ratio during dawn", function()
		assert.equals(0.36, day_cycles.daynight_ratio(5000))
	end)

	it("day/night ratio just before dusk", function()
		assert.equals(1, day_cycles.daynight_ratio(17875))
	end)

	it("day/night ratio just after dusk", function()
		assert.equals(0.15, day_cycles.daynight_ratio(19375))
	end)

	it("day/night ratio during dawn", function()
		assert.equals(0.36, day_cycles.daynight_ratio(19000))
	end)
end)


describe("set_dawn", function()
	it("set dawn at -500", function()
		day_cycles.set_dawn(-500)
		assert.equals(0, day_cycles.dawn_start)
		assert.equals(1500, day_cycles.dawn_end)
	end)

	it("set dawn at 0", function()
		day_cycles.set_dawn(0)
		assert.equals(0, day_cycles.dawn_start)
		assert.equals(1500, day_cycles.dawn_end)
	end)

	it("set dawn at 12500", function()
		day_cycles.set_dawn(12500)
		assert.equals(11999, day_cycles.dawn_start)
		assert.equals(12000, day_cycles.dawn_end)
	end)

	it("set dawn at 12000", function()
		day_cycles.set_dawn(12000)
		assert.equals(11999, day_cycles.dawn_start)
		assert.equals(12000, day_cycles.dawn_end)
	end)

	it("set dawn at 3000", function()
		day_cycles.set_dawn(3000)
		assert.equals(3000, day_cycles.dawn_start)
		assert.equals(4500, day_cycles.dawn_end)
	end)

	it("set dawn at 3000, end at 12500", function()
		day_cycles.set_dawn(3000, 12500)
		assert.equals(3000, day_cycles.dawn_start)
		assert.equals(12000, day_cycles.dawn_end)
	end)

	it("set dawn at 3000, end at 12000", function()
		day_cycles.set_dawn(3000, 12000)
		assert.equals(3000, day_cycles.dawn_start)
		assert.equals(12000, day_cycles.dawn_end)
	end)

	it("set dawn at 3000, end at 0", function()
		day_cycles.set_dawn(3000, 0)
		assert.equals(3000, day_cycles.dawn_start)
		assert.equals(3001, day_cycles.dawn_end)
	end)

	it("set dawn at 3000, end at 3000", function()
		day_cycles.set_dawn(3000, 3000)
		assert.equals(3000, day_cycles.dawn_start)
		assert.equals(3001, day_cycles.dawn_end)
	end)

	it("set dawn at 3000, end at 4000", function()
		day_cycles.set_dawn(3000, 4000)
		assert.equals(3000, day_cycles.dawn_start)
		assert.equals(4000, day_cycles.dawn_end)
	end)
end)


describe("set_day_brightness", function()
	it("set negative brightness", function()
		day_cycles.set_day_brightness(-0.5)
		assert.equals(0, day_cycles.day_light)
	end)

	it("set brightness too high", function()
		day_cycles.set_day_brightness(1.5)
		assert.equals(1, day_cycles.day_light)
	end)

	it("set brightness 0.5", function()
		day_cycles.set_day_brightness(0.5)
		assert.equals(0.5, day_cycles.day_light)
	end)
end)


describe("set_night_brightness", function()
	it("set negative brightness", function()
		day_cycles.set_night_brightness(-0.5)
		assert.equals(0, day_cycles.night_light)
	end)

	it("set brightness too high", function()
		day_cycles.set_night_brightness(1.5)
		assert.equals(1, day_cycles.night_light)
	end)

	it("set brightness 0.5", function()
		day_cycles.set_night_brightness(0.5)
		assert.equals(0.5, day_cycles.night_light)
	end)
end)



